﻿/*
 * This file is part of Arizona Sunshine Skinetic Mod.
 *
 * Arizona Sunshine Skinetic Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Skinetic Mod Template is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Arizona Sunshine Skinetic Mod.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using MelonLoader;
using Skinetic;
using UnityEngine;

namespace ArizonaSunshine_SkineticMod
{
    public class AS_SkineticMod : MelonMod
    {
        private static SkineticManager SkineticHandler;
        private static bool LowLifeEffectPlaying = false;
        private static int LowLifeEffectId = -1;

        #region Skinetic Handling
        public override void OnInitializeMelon()
        {
            LoggerInstance.Msg("OnInitializeMelon");
            SkineticHandler = new SkineticManager();
        }

        public override void OnApplicationQuit()
        {
            LoggerInstance.Msg("OnApplicationQuit");
            SkineticHandler.Disconnect();
        }

        // Add this patch because override of OnApplicationQuit is not always called by Arizona Sunshine
        [HarmonyPatch(typeof(JW_Game), "QuitApplication")]
        class CustomQuit
        {
            [HarmonyPrefix]
            static void Prefix()
            {
                Melon<AS_SkineticMod>.Logger.Msg("QuitApplication");
                SkineticHandler.Disconnect();
            }
        }
        #endregion

        #region Weapon Related interactions
        [HarmonyPatch]
        class Recoil
        {
            [HarmonyPatch(typeof(Gun), "ShootBullet")] // can get boolean result of shooting
            [HarmonyPrefix]
            static void GunRecoil(Gun __instance)
            {
                Patterns Options = __instance.EquipmentSlot.SlotID == E_EQUIPMENT_SLOT_ID.RIGHT_HAND ? Patterns.Right : Patterns.Left;
                if (!__instance.InfiniteAmmo && __instance.AmmoRemaining <= 0)
                {
                    Melon<AS_SkineticMod>.Logger.Msg("Recoil - Gun - ShootBullet - EMPTY");
                    Options |= Patterns.Empty;
                }

                Options = (__instance.IsTwoHanded && __instance.IsTwoHandedOffHandAttached) ? Patterns.TwoHanded | Options : Options;

                switch (__instance.AmmoType)
                {
                    case E_AMMO_TYPE.AMMO_BULLET: // pistol
                        SkineticHandler.Play(Patterns.GunRecoil | Options, 85, 1, 3);
                        break;
                    case E_AMMO_TYPE.AMMO_MACHINE: // rifle + smg + machinegun
                        SkineticHandler.Play(Patterns.RifleRecoil | Options, 100, 1, 3);
                        break;
                    case E_AMMO_TYPE.AMMO_SNIPER:
                        SkineticHandler.Play(Patterns.SniperRecoil | Options, 100, 1, 3);
                        break;
                    case E_AMMO_TYPE.AMMO_SHELL: // shotgun
                        SkineticHandler.Play(Patterns.ShotGunRecoil | Options, 100, 1, 3);
                        break;
                    case E_AMMO_TYPE.AMMO_GRENADE: // grenade launcher
                        SkineticHandler.Play(Patterns.GunRecoil | Options, 85, 1, 3);
                        break;
                }
                Melon<AS_SkineticMod>.Logger.Msg("Recoil - Gun - ShootBullet: " + __instance.AmmoType + " - hand (2: " + __instance.IsTwoHanded + "|" + __instance.IsTwoHandedOffHandAttached 
                    + " - left: " + __instance.HandPoseName + "|" + __instance.OffHandHandPoseName + "|" + __instance.EquipmentSlot.SlotID + ")");
            }
        }

        [HarmonyPatch]
        class GunReload
        {
            // Retrieve ammo from belt
            [HarmonyPatch(typeof(AmmoDispenserItem), nameof(AmmoDispenserItem.RequestAmmoClip), 
                new Type[] { typeof(E_ITEM_ID), typeof(E_AMMO_TYPE), typeof(AmmoClipItem)}, 
                new ArgumentType[] { ArgumentType.Normal, ArgumentType.Normal, ArgumentType.Out})]
            [HarmonyPostfix]
            static void RetrieveAmmoFromBelt(AmmoDispenserBehaviour __instance, E_ITEM_ID id, E_AMMO_TYPE ammoType, AmmoClipItem ammoClipItem)
            {
                // TODO: can discrimate type of ammo to trigger different effects
                Melon<AS_SkineticMod>.Logger.Msg("RequestAmmoClip: " + id + " - " + ammoType + " - " + ammoClipItem.HasBeenInserted);
                SkineticHandler.Play(Patterns.RetrieveAmmo, 100, 1, 7);
            }
            
            [HarmonyPatch(typeof(AmmoDispenserItem), nameof(AmmoDispenserItem.InsertAmmoClip), new Type[] { typeof(AmmoClipItem)})]
            [HarmonyPrefix]
            static void InsertAmmoClip(AmmoDispenserItem __instance, AmmoClipItem ammoClip)
            {
                // TODO: can discrimate type of ammo to trigger different effects
                Melon<AS_SkineticMod>.Logger.Msg("InsertAmmoClip: " + __instance.EquipmentSlot.SlotID + " - " + ammoClip.EquipmentSlot.SlotID + " : " + ammoClip.ItemType);
                SkineticHandler.Play(Patterns.StoreAmmo, 100, 1, 8);
            }
        }
        #endregion

        #region Damage Taken
        [HarmonyPatch(typeof(Player), "Hit", new Type[] { typeof(float) })]
        class Hit_Damage
        {
            [HarmonyPostfix]
            static void Postfix(Player __instance, float amount)
            {
                //Melon<AS_SkineticMod>.Logger.Msg("Hit: " + amount);
                if (!__instance.IsLocalPlayer || __instance.IsDead)
                    return;
                HandleLowHealth(__instance.Health);
            }
        }

        // ZombieHit
        [HarmonyPatch(typeof(Player), "ZombieHit", new Type[] { typeof(float), typeof(Zombie) })]
        class ZombieHit
        {
            //static float fireRate = 0.1f;
            //private static float lastShot = 0.0f;

            [HarmonyPrefix]
            static void Prefix(Player __instance, Zombie zombie)
            {
                if (!__instance.IsLocalPlayer || __instance.IsDead)
                    return;

                Melon<AS_SkineticMod>.Logger.Msg("ZombieHit: " + zombie.Position);

                //if (Time.time > fireRate + lastShot)
                //{
                //    lastShot = Time.time;

                ////Melon<AS_SkineticMod>.Logger.Msg("        - player pos: " + __instance.Transform.position + " | player rotation: " 
                //+ __instance.BaseRotation.eulerAngles + " | player head rot: " + __instance.HeadRotation.eulerAngles);

                Vector3 hitPosition = zombie.Position - __instance.Transform.position;
                Quaternion playerRotation = __instance.HeadRotation;
                float height = 0;

                Vector3 hit = new Vector3(hitPosition.x, 0f, hitPosition.z);
                float otherAngle = Vector3.SignedAngle(Vector3.forward, hit.normalized, Vector3.up);
                var res = otherAngle - playerRotation.eulerAngles.y;
                if (zombie.Locomotion.IsCrawling)
                    height = -0.1f;

                SkineticHandler.Play(Patterns.ZombieHit, 175, 1, 1, height, -res, 0);
                Melon<AS_SkineticMod>.Logger.Msg("        - ANGLE: " + otherAngle + " - " + playerRotation.eulerAngles.y + " = " + res);
                //}

            }
        }

        // Explosion damage
        [HarmonyPatch(typeof(PlayerHittableBehaviour), "HandleExplosiveHit", new Type[] { typeof(Vector3), typeof(float), typeof(float) })]
        class Explosion
        {
            [HarmonyPrefix]
            static void Prefix()
            {
                SkineticHandler.Play(Patterns.Explosion, 100, 1, 2, 0, 0);
                Melon<AS_SkineticMod>.Logger.Msg("HandleExplosiveHit");
            }
        }
        #endregion

        #region Health related interactions

        [HarmonyPatch(typeof(Player), "Kill")]
        class Killed
        {
            [HarmonyPrefix]
            static void Prefix(Player __instance)
            {
                if (!__instance.IsLocalPlayer) return;
                HandleLowHealth(100);
                SkineticHandler.Play(Patterns.Dead, 100, 1, 1);
                Melon<AS_SkineticMod>.Logger.Msg("Death - Kill");
            }
        }

        [HarmonyPatch(typeof(ConsumableItem), "StartConsume", new Type[] { typeof(bool)})]
        class Eating
        {
            [HarmonyPrefix]
            static void Prefix(ConsumableItem __instance, bool consumeLocal)
            {
                Melon<AS_SkineticMod>.Logger.Msg("Eating - " + consumeLocal + " - " + __instance.ItemID + " - " + __instance.ItemType); //// HERE
                if (consumeLocal && (__instance.ItemID == E_ITEM_ID.HAMBURGER_COOKED || __instance.ItemID == E_ITEM_ID.HAMBURGER_RAW))
                {
                    SkineticHandler.Play(Patterns.Eating, 100, 1, 8);
                }
            }
        }

        // start continous heartbeat effect if health < 30 - stop it when back to > 30
        public static void HandleLowHealth(float health)
        {
            if (health < 30.0f && !LowLifeEffectPlaying)
            {
                Melon<AS_SkineticMod>.Logger.Msg("START LowLife effect - " + health);
                LowLifeEffectId = SkineticHandler.Play(Patterns.LowLife, 100, 1, 7);
                LowLifeEffectPlaying = true;
            }
            else if (health >= 30.0f && LowLifeEffectPlaying)
            {
                Melon<AS_SkineticMod>.Logger.Msg("STOP LowLife effect - " + health);
                SkineticHandler.Stop(LowLifeEffectId, 0.2f);
                LowLifeEffectPlaying = false;
            }
        }
        #endregion

        #region item handling
        [HarmonyPatch]
        class StoreItem
        {
            //[HarmonyPatch(typeof(Equipment), "GenerateEquipment", new Type[] { typeof(EquipmentData), typeof(bool)})]
            //[HarmonyPrefix]
            //static void GenerateEquipment(EquipmentData __instance, EquipmentData equipmentTO, bool enableAmmoDispenser)
            //{
            //    Melon<AS_SkineticMod>.Logger.Msg(" +++ GenerateEquipment: " + equipmentTO + " +++ ");
            //}

            [HarmonyPatch(typeof(ExplosiveItem), "OnEquipped", new Type[] { typeof(EquipmentSlot), typeof(bool) })]
            [HarmonyPrefix]
            static void OnEquipped(ExplosiveItem __instance, EquipmentSlot equipmentSlot, bool isSpawnEquip)
            {
                Melon<AS_SkineticMod>.Logger.Msg(" +++ ExplosiveItem::OnEquipped: " + __instance.ItemID + " --- " +  __instance.ItemType + " => " + equipmentSlot.SlotID + " +++ ");
                if (equipmentSlot.SlotID == E_EQUIPMENT_SLOT_ID.CHEST_UPPER || equipmentSlot.SlotID == E_EQUIPMENT_SLOT_ID.CHEST_LOWER)
                    SkineticHandler.Play(Patterns.StoreItem);
            }

            [HarmonyPatch(typeof(InventoryItem), "AddToInventory", new Type[] { typeof(Inventory), typeof(bool) })]
            [HarmonyPostfix]
            static void AddToInventory(InventoryItem __instance, Inventory inventory, bool lerpToInventory)
            {
                Melon<AS_SkineticMod>.Logger.Msg(" +++ InventoryItem::AddToInventory: " + __instance.ItemID + " --- " +  __instance.ItemType);

                if (__instance.ItemID != E_ITEM_ID.KEY || __instance.ItemID != E_ITEM_ID.WRENCH || __instance.ItemID != E_ITEM_ID.WINCH)
                    return;
                SkineticHandler.Play(Patterns.StoreItem); 
            }
        }
        #endregion
    }
}
