﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using MelonLoader;
using ArizonaSunshine_SkineticMod;

[assembly: MelonInfo(typeof(AS_SkineticMod), "Arizona Sunshine - Skinetic Mod", "1.1.0", "Actronika SAS")]
[assembly: MelonGame("Vertigo Games", "ArizonaSunshine")]


// Les informations générales relatives à un assembly dépendent de
// l'ensemble d'attributs suivant. Changez les valeurs de ces attributs pour modifier les informations
// associées à un assembly.
[assembly: AssemblyTitle("ArizonaSunshine_SkineticMod")]
[assembly: AssemblyDescription("Add support for Skinetic haptic vest")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Actronika SAS")]
[assembly: AssemblyProduct("ArizonaSunshine_SkineticMod")]
[assembly: AssemblyCopyright("Copyright © 2023")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// L'affectation de la valeur false à ComVisible rend les types invisibles dans cet assembly
// aux composants COM. Si vous devez accéder à un type dans cet assembly à partir de
// COM, affectez la valeur true à l'attribut ComVisible sur ce type.
[assembly: ComVisible(false)]

// Le GUID suivant est pour l'ID de la typelib si ce projet est exposé à COM
[assembly: Guid("5f4ad8f5-e85d-414d-a7d6-2fe3fe60a734")]

// Les informations de version pour un assembly se composent des quatre valeurs suivantes :
//
//      Version principale
//      Version secondaire
//      Numéro de build
//      Révision
//
// Vous pouvez spécifier toutes les valeurs ou indiquer les numéros de build et de révision par défaut
// en utilisant '*', comme indiqué ci-dessous :
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0")]
[assembly: AssemblyFileVersion("1.1.0")]
